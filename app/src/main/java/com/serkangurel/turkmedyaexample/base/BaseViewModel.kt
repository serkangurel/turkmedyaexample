package com.serkangurel.turkmedyaexample.base

import android.app.Application
import android.content.Context
import android.content.res.Resources
import androidx.lifecycle.AndroidViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    val disposable = CompositeDisposable()
    var resources: Resources
    var context: Context

    init {
        resources = application.resources
        context = application.applicationContext
    }

    override fun onCleared() {
        super.onCleared()
        if (disposable.size() > 0) {
            disposable.dispose()
        }
    }
}