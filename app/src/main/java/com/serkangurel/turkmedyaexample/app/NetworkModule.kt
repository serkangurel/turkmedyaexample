package com.serkangurel.turkmedyaexample.app

import com.google.gson.GsonBuilder
import com.serkangurel.turkmedyaexample.common.Constants
import com.serkangurel.turkmedyaexample.data.service.NewsApiService
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object NetworkModule {

    val networkModules = module {
        single { provideDefaultOkhttpClient() }
        single { provideRetrofit(get()) }
        single { provideService(get()) }
    }

    private fun provideDefaultOkhttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .build()
    }

    private fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(client)
            .baseUrl(Constants.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
    }

    private fun provideService(retrofit: Retrofit): NewsApiService {
        return retrofit.create(NewsApiService::class.java)
    }

}