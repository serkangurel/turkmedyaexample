package com.serkangurel.turkmedyaexample.app

import com.serkangurel.turkmedyaexample.ui.fragment.detail.DetailViewModel
import com.serkangurel.turkmedyaexample.ui.fragment.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object AppModules {

    val appModules: Module = module {
        viewModel { HomeViewModel(get(), get()) }
        viewModel { DetailViewModel(get(), get()) }
    }
}