package com.serkangurel.turkmedyaexample.notification

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.serkangurel.turkmedyaexample.data.model.LiveStream
import com.serkangurel.turkmedyaexample.ui.MainActivity

class StreamService : Service() {

    var notificationGenerator: NotificationGenerator =
        NotificationGenerator(MainActivity::class.java)

    lateinit var stream: LiveStream

    var player = Player()

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.apply {
            when (action) {
                Status.START -> {
                    stream = intent.getParcelableExtra(Status.PARAM_STREAM)
                    startService()
                }
                Status.STOP -> {
                    stop()
                }
                Status.PAUSE -> {
                    pause()
                }
                Status.RESUME -> {
                    resume()
                }
            }
        }
        return START_NOT_STICKY
    }

    fun startService() {
        startForeground(
            NOTIFICATION_ID,
            notificationGenerator.getNotification(
                this,
                stream.streamTitle,
                true
            )
        )
        play()
    }

    fun play() {
        player.play(stream)
        notificationGenerator.updateNotification(
            this,
            notificationGenerator.getNotification(
                this,
                stream.streamTitle,
                true
            )
        )
    }

    fun pause() {
        player.pause()
        notificationGenerator.updateNotification(
            this,
            notificationGenerator.getNotification(
                this,
                stream.streamTitle,
                false
            )
        )
    }

    fun resume() {
        player.resume()
        notificationGenerator.updateNotification(
            this,
            notificationGenerator.getNotification(
                this,
                stream.streamTitle,
                true
            )
        )
    }

    fun stop() {
        player.stop()
        stopSelf()
    }

    override fun onUnbind(intent: Intent?): Boolean {
        stop()
        return super.onUnbind(intent)
    }
}