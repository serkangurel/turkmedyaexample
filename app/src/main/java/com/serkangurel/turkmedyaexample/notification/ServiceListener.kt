package com.serkangurel.turkmedyaexample.notification

interface ServiceListener {
    fun OnServiceEnabled()
    fun OnServiceDisabled()
}