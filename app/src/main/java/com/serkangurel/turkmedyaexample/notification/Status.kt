package com.serkangurel.turkmedyaexample.notification

object Status {
    private const val PATH = "com.serkangurel.turkmedyaexample.action."

    const val START = PATH + "START"
    const val STOP = PATH + "STOP"
    const val PAUSE = PATH + "PAUSE"
    const val RESUME = PATH + "RESUME"

    const val PARAM_STREAM = "PARAM_STREAM"
}