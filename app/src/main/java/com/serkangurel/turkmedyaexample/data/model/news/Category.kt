package com.serkangurel.turkmedyaexample.data.model.news

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category(
    val categoryId: String?,
    val color: String?,
    val slug: String?,
    val title: String?
) : Parcelable