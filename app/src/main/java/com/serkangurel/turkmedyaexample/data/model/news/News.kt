package com.serkangurel.turkmedyaexample.data.model.news

import android.os.Parcelable
import com.serkangurel.turkmedyaexample.data.model.RVModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class News(
    val data: List<Data>?,
    val errorCode: Int?,
    val errorMessage: String?
) : RVModel, Parcelable