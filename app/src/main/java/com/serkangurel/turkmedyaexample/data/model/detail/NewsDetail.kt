package com.serkangurel.turkmedyaexample.data.model.detail

import com.serkangurel.turkmedyaexample.data.model.news.Category

data class NewsDetail(
    val bodyText: String?,
    val category: Category?,
    val fullPath: String?,
    val hasPhotoGallery: Boolean?,
    val hasVideo: Boolean?,
    val imageUrl: String?,
    val itemId: String?,
    val itemType: String?,
    val publishDate: String?,
    val resource: String?,
    val shortText: String?,
    val title: String?,
    val video: String?
)