package com.serkangurel.turkmedyaexample.data.model.detail

data class Data(
    val footerAd: FooterAd?,
    val headerAd: HeaderAd?,
    val itemList: List<Item>?,
    val multimedia: Multimedia?,
    val newsDetail: NewsDetail?,
    val photoGallery: PhotoGallery?,
    val relatedNews: RelatedNews?,
    val video: Video?
)