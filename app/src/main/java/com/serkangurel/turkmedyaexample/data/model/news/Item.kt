package com.serkangurel.turkmedyaexample.data.model.news

import android.os.Parcelable
import com.serkangurel.turkmedyaexample.data.model.RVModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Item(
    val category: Category?,
    val columnistName: String?,
    val externalUrl: String?,
    val fLike: String?,
    val fullPath: String?,
    val hasPhotoGallery: Boolean?,
    val hasVideo: Boolean?,
    val imageUrl: String?,
    val itemId: String?,
    val itemType: String?,
    val publishDate: String?,
    val shortText: String?,
    val title: String?,
    val titleVisible: Boolean?,
    val videoUrl: String?
) : RVModel, Parcelable