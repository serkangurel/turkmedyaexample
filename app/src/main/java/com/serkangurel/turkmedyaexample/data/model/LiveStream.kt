package com.serkangurel.turkmedyaexample.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LiveStream(
    val streamUrl: String?,
    val streamTitle: String
) : Parcelable, RVModel