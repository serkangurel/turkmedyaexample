package com.serkangurel.turkmedyaexample.data.model.detail

data class Detail(
    val data: Data?,
    val errorCode: Int?,
    val errorMessage: Any?
)