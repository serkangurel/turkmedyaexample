package com.serkangurel.turkmedyaexample.data.model.detail

data class Item(
    val bodyText: String?,
    val imageUrl: String?,
    val itemId: String?,
    val itemList: Any?,
    val itemType: String?,
    val shortText: String?,
    val title: String?,
    val titleVisible: Boolean?,
    val videoUrl: String?
)