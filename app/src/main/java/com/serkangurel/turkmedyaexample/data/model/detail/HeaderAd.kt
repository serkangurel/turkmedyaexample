package com.serkangurel.turkmedyaexample.data.model.detail

data class HeaderAd(
    val adUnit: String?,
    val itemHeight: Int?,
    val itemType: String?,
    val itemWidth: Int?
)