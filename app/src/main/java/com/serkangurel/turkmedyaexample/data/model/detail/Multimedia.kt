package com.serkangurel.turkmedyaexample.data.model.detail

data class Multimedia(
    val itemCountInRow: Int?,
    val lazyLoadingEnabled: Boolean?,
    val repeatType: String?,
    val sectionBgColor: Any?,
    val sectionType: String?,
    val title: Any?,
    val titleBgColor: Any?,
    val titleColor: Any?,
    val titleVisible: Boolean?
)