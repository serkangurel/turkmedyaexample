package com.serkangurel.turkmedyaexample.data.model.news

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(
    val itemCountInRow: Int?,
    val itemList: List<Item>?,
    val lazyLoadingEnabled: Boolean?,
    val repeatType: String?,
    val sectionBgColor: String?,
    val sectionType: String?,
    val title: String?,
    val titleBgColor: String?,
    val titleColor: String?,
    val titleVisible: Boolean?,
    val totalRecords: Int?
) : Parcelable