package com.serkangurel.turkmedyaexample.data.model.detail

data class Video(
    val bodyText: String?,
    val imageUrl: String?,
    val itemId: String?,
    val itemType: String?,
    val shortText: String?,
    val title: String?,
    val titleVisible: Boolean?,
    val videoUrl: String?
)