package com.serkangurel.turkmedyaexample.data.model.detail

import com.serkangurel.turkmedyaexample.data.model.news.Category

data class RelatedNews(
    val category: Category?,
    val hasPhotoGallery: Boolean?,
    val hasVideo: Boolean?,
    val imageUrl: String?,
    val itemId: String?,
    val itemType: String?,
    val publishDate: String?,
    val shortText: String?,
    val title: String?,
    val titleVisible: Boolean?
)