package com.serkangurel.turkmedyaexample.data.service

import com.serkangurel.turkmedyaexample.data.model.detail.Detail
import com.serkangurel.turkmedyaexample.data.model.news.News
import io.reactivex.Single
import retrofit2.http.GET

interface NewsApiService {

    @GET("anasayfa.json")
    fun getNews(): Single<News>

    @GET("detay.json")
    fun getDetails(): Single<Detail>
}