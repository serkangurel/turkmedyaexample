package com.serkangurel.turkmedyaexample.data.model.detail

data class PhotoGallery(
    val imageUrl: String?,
    val itemId: String?,
    val itemList: Any?,
    val itemType: String?,
    val title: String?,
    val titleVisible: Boolean?
)