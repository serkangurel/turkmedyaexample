package com.serkangurel.turkmedyaexample.ui.fragment.detail

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.serkangurel.turkmedyaexample.base.BaseViewModel
import com.serkangurel.turkmedyaexample.common.plusAssign
import com.serkangurel.turkmedyaexample.data.model.detail.Detail
import com.serkangurel.turkmedyaexample.data.service.NewsApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetailViewModel(
    application: Application,
    private val newsApiService: NewsApiService
) :
    BaseViewModel(application) {

    companion object {
        private const val TAG = "DetailViewModel"
    }

    private var _detailLiveData = MutableLiveData<Detail>()
    val detailLiveData: LiveData<Detail>
        get() = _detailLiveData

    private var _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    fun fetchDetails() {
        disposable += newsApiService.getDetails()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleResponse, this::handleError)
    }

    private fun handleResponse(news: Detail) {
        _detailLiveData.postValue(news)
    }

    private fun handleError(error: Throwable) {
        Log.e(TAG, "handleError: ${error.message}")
        _errorMessage.postValue(error.message)
    }
}