package com.serkangurel.turkmedyaexample.ui.fragment.home

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.serkangurel.turkmedyaexample.base.BaseViewModel
import com.serkangurel.turkmedyaexample.common.plusAssign
import com.serkangurel.turkmedyaexample.data.model.news.News
import com.serkangurel.turkmedyaexample.data.service.NewsApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeViewModel(
    application: Application,
    private val newsApiService: NewsApiService
) :
    BaseViewModel(application) {

    companion object {
        private const val TAG = "HomeViewModel"
    }

    private var _newsLiveData = MutableLiveData<News>()
    val newsLiveData: LiveData<News>
        get() = _newsLiveData

    private var _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    fun fetchNews() {
        disposable += newsApiService.getNews()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleResponse, this::handleError)
    }

    private fun handleResponse(news: News) {
        _newsLiveData.postValue(news)
    }

    private fun handleError(error: Throwable) {
        Log.e(TAG, "handleError: ${error.message}")
        _errorMessage.postValue(error.message)
    }
}