package com.serkangurel.turkmedyaexample.ui.fragment.home

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.serkangurel.turkmedyaexample.R
import com.serkangurel.turkmedyaexample.adapter.ItemRVA
import com.serkangurel.turkmedyaexample.common.Constants
import com.serkangurel.turkmedyaexample.common.observe
import com.serkangurel.turkmedyaexample.common.showToast
import com.serkangurel.turkmedyaexample.data.model.LiveStream
import com.serkangurel.turkmedyaexample.data.model.RVModel
import com.serkangurel.turkmedyaexample.databinding.HomeFragmentBinding
import com.serkangurel.turkmedyaexample.notification.ServiceListener
import com.serkangurel.turkmedyaexample.notification.Status
import com.serkangurel.turkmedyaexample.notification.StreamService
import org.koin.android.ext.android.inject


class HomeFragment : Fragment(), ServiceListener {

    private lateinit var binding: HomeFragmentBinding
    private val viewModel: HomeViewModel by inject()
    private var isViewsCreated = false
    private var adaper: ItemRVA? = null
    private var stream: LiveStream? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (isViewsCreated) return binding.root
        isViewsCreated = true

        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        binding.lifecycleOwner = this

        binding.viewModel = viewModel

        stream = LiveStream(Constants.LIVE_STREAM_URL, Constants.LIVE_STREAM_TITLE)

        viewModel.fetchNews()

        adaper = ItemRVA(requireContext(), this)

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.reyclerview.layoutManager = layoutManager
        binding.reyclerview.adapter = adaper

        observe(viewModel.newsLiveData) { news ->
            binding.progressBarLoading.visibility = View.GONE

            val list = arrayListOf<RVModel>()
            stream?.let { list.add(it) }
            news?.data?.get(1)?.itemList?.let { list.addAll(it) }

            adaper?.setList(list)
        }

        observe(viewModel.errorMessage) { errorMessage ->
            binding.progressBarLoading.visibility = View.GONE
            context?.showToast(errorMessage)
        }

        return binding.root
    }

    fun startStream(stream: LiveStream) {
        val intent = Intent(requireContext(), StreamService::class.java)
        intent.putExtra(Status.PARAM_STREAM, stream)
        intent.action = Status.START
        ContextCompat.startForegroundService(requireContext(), intent)
    }

    fun stopStream() {
        val intent = Intent(requireContext(), StreamService::class.java)
        intent.action = Status.STOP
        ContextCompat.startForegroundService(requireContext(), intent)
    }


    @Suppress("DEPRECATION")
    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = context?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    override fun onResume() {
        super.onResume()
        adaper?.startExoplayer()
    }

    override fun onPause() {
        super.onPause()
        adaper?.stopExoplayer()
    }

    override fun OnServiceEnabled() {
        if (!isMyServiceRunning(StreamService::class.java))
            stream?.let { startStream(it) }
    }

    override fun OnServiceDisabled() {
        if (isMyServiceRunning(StreamService::class.java))
            stopStream()
    }

}