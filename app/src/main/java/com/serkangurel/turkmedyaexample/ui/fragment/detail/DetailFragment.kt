package com.serkangurel.turkmedyaexample.ui.fragment.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.serkangurel.turkmedyaexample.R
import com.serkangurel.turkmedyaexample.common.observe
import com.serkangurel.turkmedyaexample.common.showToast
import com.serkangurel.turkmedyaexample.data.model.detail.Detail
import com.serkangurel.turkmedyaexample.databinding.DetailFragmentBinding
import org.koin.android.ext.android.inject

class DetailFragment : Fragment() {

    private lateinit var binding: DetailFragmentBinding
    private val viewModel: DetailViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.detail_fragment, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = viewModel

        viewModel.fetchDetails()

        observe(viewModel.detailLiveData) { detail ->
            binding.progressBarLoading.visibility = View.GONE
            binding.svContent.visibility = View.VISIBLE
            setViews(detail)
        }

        observe(viewModel.errorMessage) { errorMessage ->
            binding.progressBarLoading.visibility = View.GONE
            context?.showToast(errorMessage)
        }

    }

    private fun setViews(detail: Detail?) {
        val newsDetail = detail?.data?.newsDetail

        if (newsDetail != null) {

            // apiden gelen imageUrl bozuk olduğundan haber sitesindeki resmin url'ini kullandım.
            val imageUrl =
                "https://img3.aksam.com.tr/imgsdisk/2019/05/27/270520190933193893628_2.jpg"

            Glide.with(requireContext())
                .load(imageUrl)
                .into(binding.ivPhoto)

            binding.tvTitle.text = newsDetail.title
            binding.tvCategory.text = newsDetail.category?.title
            binding.tvDate.text = newsDetail.publishDate
            binding.tvShortDescription.text = newsDetail.shortText
            binding.tvDescription.text = newsDetail.bodyText

        }

    }

}