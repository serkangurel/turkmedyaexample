package com.serkangurel.turkmedyaexample.common

object Constants {
    const val BASE_URL = "https://turkmedya.com.tr/"
    const val LIVE_STREAM_URL = "http://mn-nl.mncdn.com/kanal24/smil:kanal24.smil/playlist.m3u8"
    const val LIVE_STREAM_TITLE = "24 TV Canlı Yayın"
}