package com.serkangurel.turkmedyaexample.common

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Handler
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.IntRange
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.*
import kotlin.collections.ArrayList

inline fun <reified T : Annotation> Any.getAnnotation(
    isRequired: Boolean = false,
    block: (T) -> Unit = {}
): T? {
    var ann: T? = null
    if (javaClass.isAnnotationPresent(T::class.java)) {
        ann = javaClass.getAnnotation(T::class.java)
        ann?.let { block(it) }
    } else {
        if (isRequired) {
            throw RuntimeException("Annotation : ${javaClass.name} not found")
        }
    }
    return ann
}

operator fun CompositeDisposable.plusAssign(disposable: Disposable) {
    add(disposable)
}

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) {
    liveData.observe(this, Observer(body))
}


fun Context.showToast(message: String?, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}

fun isNullableEqual(obj1: Any?, obj2: Any?): Boolean {
    return if (obj1 == null) {
        obj2 == null
    } else obj2 != null && obj2.equals(obj1)
}

fun Context.getColorRes(
    @ColorRes resColorId: Int, @IntRange(
        from = 0x0,
        to = 0xFF
    ) alpha: Int? = null
): Int {
    val color = ContextCompat.getColor(this, resColorId)
    return if (alpha == null) color else ColorUtils.setAlphaComponent(color, alpha)
}

inline fun <E : Any, T : Collection<E>, R : Any> T?.ifNotNullOrEmpty(block: (T) -> R?): R? {
    if (this?.isNotEmpty() == true) {
        return block(this)
    }
    return null
}

fun randomAlphaNumericString(@IntRange(from = 1, to = 62) lenght: Int): String {
    val alphaNumeric = ('a'..'z') + ('A'..'Z') + ('0'..'9')
    return alphaNumeric.shuffled().take(lenght).joinToString("")
}

fun Context.getColorRes(@ColorRes resColorId: Int): Int {
    return ContextCompat.getColor(this, resColorId)
}

fun runDelayed(delay: Long, action: () -> Unit) {
    Handler().postDelayed(action, delay)
}

fun <K, M : K> List<M>.castToSubType(): List<K> {
    return ArrayList<K>(this)
}

@SuppressLint("DefaultLocale")
fun String.capitalizeWords(): String = split(" ").joinToString(" ") {
    it.toLowerCase(Locale.getDefault()).capitalize()
}

fun hideSoftKeyboard(context: Context) {
    if (context is Activity) {
        val view = context.currentFocus
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE)
        if (view != null && imm is InputMethodManager) {
            imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }
}