package com.serkangurel.turkmedyaexample.adapter

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.LoadControl
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelection
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.serkangurel.turkmedyaexample.R
import com.serkangurel.turkmedyaexample.common.Constants
import com.serkangurel.turkmedyaexample.data.model.LiveStream
import com.serkangurel.turkmedyaexample.data.model.RVModel
import com.serkangurel.turkmedyaexample.data.model.news.Item
import com.serkangurel.turkmedyaexample.notification.ServiceListener

class ItemRVA(
    context: Context,
    listener: ServiceListener? = null,
    list: List<RVModel>? = null
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val itemList = arrayListOf<RVModel>()
    private var context: Context
    private var exoPlayer: PlayerView? = null
    private var listener: ServiceListener? = null

    init {
        list?.let { itemList.addAll(it) }
        this.listener = listener
        this.context = context
    }

    companion object {
        private const val LIVE_STREAM = 1
        private const val NEWS = 2
    }

    fun setList(list: List<RVModel>?) {
        if (list != null) {
            itemList.clear()
            itemList.addAll(list)
            notifyDataSetChanged()
        }
    }

    fun addList(list: List<RVModel>?) {
        if (list != null) {
            val pos = itemCount
            itemList.addAll(list)
            notifyItemRangeInserted(pos, list.size)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (itemList[position]) {
            is LiveStream -> LIVE_STREAM
            is Item -> NEWS
            else -> -1
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            LIVE_STREAM -> {
                val view = inflater.inflate(R.layout.item_stream, parent, false)
                StreamViewHolder(view)
            }
            NEWS -> {
                val view = inflater.inflate(R.layout.item_news, parent, false)
                NewsViewHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid viewType : $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            NEWS -> {
                val newsViewHolder = holder as NewsViewHolder
                val newsItem = itemList[position] as Item

                Glide.with(context)
                    .load(newsItem.imageUrl)
                    .into(newsViewHolder.ivPhoto)

                newsViewHolder.tvTitle.text = newsItem.title
                newsViewHolder.tvCategory.text = newsItem.category?.title
                newsViewHolder.tvDate.text = newsItem.publishDate

                holder.itemView.setOnClickListener { view ->
                    view.findNavController().navigate(R.id.action_homeFragment_to_detailFragment)
                }
            }
        }
    }

    private fun playExoplayer(playerView: PlayerView, url: String) {
        stopStreamService()

        val bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()
        val videoFactory: TrackSelection.Factory =
            AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector: TrackSelector = DefaultTrackSelector(videoFactory)
        val loadControl: LoadControl = DefaultLoadControl()

        val player = SimpleExoPlayer.Builder(context)
            .setBandwidthMeter(bandwidthMeter)
            .setTrackSelector(trackSelector)
            .setLoadControl(loadControl)
            .build()

        val userAgent = Util.getUserAgent(context, "User Agent")
        val dataSourceFactory: DataSource.Factory = DefaultHttpDataSourceFactory(userAgent)

        val source = HlsMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(url))
        player.playWhenReady = true
        player.prepare(source)

        playerView.setShutterBackgroundColor(Color.TRANSPARENT)
        playerView.player = player
        playerView.requestFocus()
    }

    fun startExoplayer() {
        exoPlayer?.let { player ->
            playExoplayer(player, Constants.LIVE_STREAM_URL)
        }
    }

    fun stopExoplayer() {
        exoPlayer?.player?.stop()
        startStreamService()
    }

    private fun startStreamService() {
        listener?.OnServiceEnabled()
    }

    private fun stopStreamService() {
        listener?.OnServiceDisabled()
    }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        if (holder is StreamViewHolder) {
            val streamItem = itemList[0] as LiveStream
            exoPlayer = holder.playerView
            playExoplayer(holder.playerView, streamItem.streamUrl!!)
        }
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        if (holder is StreamViewHolder) {
            exoPlayer = null
            holder.playerView.player?.stop()
            startStreamService()
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class StreamViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val playerView: PlayerView

        init {
            playerView = view.findViewById(R.id.playerView)
        }
    }

    inner class NewsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivPhoto: ImageView
        val tvTitle: TextView
        val tvCategory: TextView
        val tvDate: TextView

        init {
            ivPhoto = view.findViewById(R.id.iv_photo)
            tvTitle = view.findViewById(R.id.tv_title)
            tvCategory = view.findViewById(R.id.tv_category)
            tvDate = view.findViewById(R.id.tv_date)
        }
    }

}