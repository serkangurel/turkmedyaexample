[APK Dosyası](https://gitlab.com/serkangurel/turkmedyaexample/-/blob/master/apk/app-debug.apk)

## Kullanılan kütüphaneler

- Koin (Dependency Injection)
- Gson
- Glide
- Retrofit
- RxJava
- ViewModel
- Navigation Component
- Exoplayer

Projede architecture olarak MVVM patterni uygulanmıştır.